#!/usr/bin/env python3
"""
Generate BBH mock samples
Zhuotao Li
"""

import gwcosmo
import numpy as np
import json
import pickle
import bilby
import h5py
import pandas as pd
import os
import sys
import argparse
import datetime

from itertools import product

from gwcosmo.utilities.posterior_utilities import str2bool
from gwcosmo.utilities.arguments import create_parser
from gwcosmo.utilities.cosmology import *
from gwcosmo.utilities.check_boundary import *
from gwcosmo.injections import injections_at_detector

from gwcosmo.prior.priors import *
from astropy.cosmology import FlatLambdaCDM

from scipy import integrate
from scipy import interpolate


def uniform_comoving_sampling(num,z_min,z_max,cosmo,bbh_evolution='None'):
    print('No catalogue: using uniform comoving prior in redshift.')
    step = 0.01
    #z_comoving = np.linspace(step, z_max, round(z_max/step))
    z_comoving = np.linspace(0, z_max, round(z_max/step)+1)
    # generate samples using inverse transform sampling
    # (1+z) is for time dilution effect
    if bbh_evolution == 'None':
        bbh_evo = 1/(1+z_comoving)
    z_pdf = np.array(cosmo.differential_comoving_volume(z_comoving).value)*bbh_evo
    #z_pdf = np.array(cosmo.differential_comoving_volume(z_comoving).value)
    norm_fac = sum(z_pdf)*step
    z_pdf = z_pdf/norm_fac
    
    ########################
    event_count = 0
    samp_num = num
    z = []
    while event_count<num:
        uniform_samples_x = (z_max-z_min)*np.random.rand(samp_num)+z_min
        uniform_samples_y = max(z_pdf)*np.random.rand(samp_num)
        pdf_at_sample = interpolate.CubicSpline(z_comoving,z_pdf)(uniform_samples_x)
        ind = np.where(uniform_samples_y<pdf_at_sample)
        z_tmp = uniform_samples_x[ind]
        z = np.concatenate((z, z_tmp), axis=0)
        event_count += len(z_tmp)
        samp_num = num-len(z)
        #n=len(z)
        one = np.ones((num,))
    return z*one

def empty_catalogue_sampling(empty_catalogue_file,num,bbh_evolution='None'):
    print('Using empty catalogue file: '+empty_catalogue_file)
    file = h5py.File(empty_catalogue_file, 'r')
    z_array = np.array(file['z_array'])
    pz_eg = np.array(file['10001'])
    index = []
    index2=[]
    index = np.array(range(len(z_array)))
    index2 = index[1:]
    index = index[:-1]
    dz = z_array[index2]-z_array[index]
    ave = (pz_eg[index2]+pz_eg[index])/2
    norm_fac3 = sum(ave*dz)
    z_pdf =np.array(pz_eg/norm_fac3)

    ########################
    event_count = 0
    samp_num = num
    z = []
    z_max = max(z_array)
    z_min = min(z_array)
    while event_count<num:
        uniform_samples_x = (z_max-z_min)*np.random.rand(samp_num)+z_min
        uniform_samples_y = max(z_pdf)*np.random.rand(samp_num)
        pdf_at_sample = interpolate.CubicSpline(z_array,z_pdf)(uniform_samples_x)
        ind = np.where(uniform_samples_y<pdf_at_sample)
        z_tmp = uniform_samples_x[ind]
        z = np.concatenate((z, z_tmp), axis=0)
        event_count += len(z_tmp)
        samp_num = num-len(z)
        #n=len(z)
        one = np.ones((num,))
    return z*one
    
def galaxy_catalogue_sampling(galaxy_catalogue_file,band,num,cosmo,bbh_evolution='None'):
    print('Using the galaxy catalogue file: '+galaxy_catalogue_file)
    f = h5py.File(galaxy_catalogue_file, 'r')
    ra = np.array(f['ra'])
    dec = np.array(f['dec'])
    z = np.array(f['z'])
    gal_id = np.array(f['gal_id'])
    dL = np.array(cosmo.luminosity_distance(z))
    
    if bbh_evolution == 'None':
        bbh_evo = 1/(1+z)
    
    if band=='None':
        weights = np.ones(len(z))*bbh_evo
    else:
        #band = 'm_'+band.lower()
        #m = np.array(f[band])
        #M = m - 5*np.log10(dL) - 25
        if str(band.lower()) in ['s','m']:
            abs_band = 'm_'+str(band.lower())
        else:
            abs_band = 'M_'+str(band.lower())
        M = np.array(f[abs_band])
        L = 3.0128e28*10**(-0.4*M)
        weights = L*bbh_evo
    weights = weights/sum(weights)
    ind = np.random.choice(len(z),num,p=weights)
    
    z_bbh = z[ind]
    ra_bbh = ra[ind]
    dec_bbh = dec[ind]
    gal_id_bbh = gal_id[ind]
    
    
    return z_bbh,ra_bbh,dec_bbh,gal_id_bbh
    
    
def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--pop_num', type=int, default=3000,
        help='Total number of the population.')
    parser.add_argument(
        '--pop_id', type=str, default=datetime.datetime.now().strftime("%d%m%Y"),
        help='The name for the population. Default is the date.')
    parser.add_argument(
        '--galaxy_catalogue_file', type=str, default="None",
        help='The galaxy catalogue file used for population generation, the default is None(i.e., empty catalogue). \
         Note that if the catalogue is on the list, the cosmological parameters will be set the same to the catalogue automatically overwriting your setup.')
    parser.add_argument(
        '--band', type=str, default='r',
        help='Which band to use for weighting host galaxy. R band by default and None means uniform weighting.')
    parser.add_argument(
        '--empty_catalogue_file', type=str, default="None",
        help='Use the empty catalogue LOS file as the p(z) for events, need galaxy_catalogue_file==None.')
    parser.add_argument(
        '--bbh_evolution', type=str, default='None',
        help='The time evolution model for BBHs.')
    parser.add_argument(
        '--z_min', type=float, default=0,
        help='Minimum of the redshift range of the population.')
    parser.add_argument(
        '--z_max', type=float, default=4,
        help='Maximum of the redshift range of the population.')
    parser.add_argument(
        '--H0', type=float, default=70.0,
        help='Value of the Hubble constant.')
    parser.add_argument(
        '--Omega_M', type=float, default=0.3065,
        help='Value of the Omega M.')
    parser.add_argument(
        '--mminbh', type=float, default=4.98,
        help='Minimum value for black hole source frame mass prior, using powerlaw gaussian model.')
    parser.add_argument(
        '--mmaxbh', type=float, default=112.5,
        help='Maximum value for black hole source frame mass prior, using powerlaw gaussian model.')
    parser.add_argument(
        '--alpha', type=float, default=3.78,
        help='Value of the m1 mass prior powerlaw index.')
    parser.add_argument(
        '--mu_g', type=float, default=32.27,
        help='Value of the mass prior gaussian mean.')
    parser.add_argument(
        '--sigma_g', type=float, default=3.88,
        help='Value of the mass prior gaussian sigma')
    parser.add_argument(
        '--lambda_peak', type=float, default=0.03,
        help='Value of the lambda peak.')
    parser.add_argument(
        '--delta_m', type=float, default=4.8,
        help='Value of the delta_m.')
    parser.add_argument(
        '--beta', type=float, default=0.81,
        help='Value of the m2 mass prior powerlaw index.')
    parser.add_argument(
        '--t_start', type=int, default=0,
        help='Value of the start time.')
    parser.add_argument(
        '--t_end', type=int, default=31536000,
        help='Value of the end time.')
    

    args = parser.parse_args()
    galaxy_catalogue_file = args.galaxy_catalogue_file
    empty_catalogue_file = args.empty_catalogue_file

    rng = np.random.default_rng()
    num = args.pop_num
    mminbh = args.mminbh
    mmaxbh = args.mmaxbh
    alpha = args.alpha
    mu_g = args.mu_g
    sigma_g = args.sigma_g
    lambda_peak = args.lambda_peak
    delta_m = args.delta_m
    beta = args.beta
    z_max = args.z_max
    z_min = args.z_min
    t_start = args.t_start
    t_end = args.t_end
    band = args.band
    bbh_evolution = args.bbh_evolution
    
    H0 = args.H0
    Omega_M = args.Omega_M 

    if galaxy_catalogue_file != 'None':
        file_path = os.path.dirname(os.path.abspath(__file__))
        with open(file_path+'/../utility/galaxy_catalogue_list.json') as json_file:
            parameter_dict = json.load(json_file)
        for key in parameter_dict:
            if key in galaxy_catalogue_file:
                H0 = parameter_dict[key]['H0']
                Omega_M = parameter_dict[key]['Omega_M']
                print("This mock catalogue, "+key+", is recorded in our list, setup the cosmological parameters(H0, OmegaM) to the according values.")

    
    cosmo = FlatLambdaCDM(H0,Omega_M)
    
    # mass generation
    mass_priors = BBH_powerlaw_gaussian(mminbh=mminbh,mmaxbh=mmaxbh,alpha=alpha,mu_g=mu_g,sigma_g=sigma_g,lambda_peak=lambda_peak,delta_m=delta_m,beta=beta)
    mass_1, mass_2 = mass_priors.sample(Nsample=num)

    
    # redshift generation
    if galaxy_catalogue_file=='None':
        print("No galaxy catalogue specified, using uniform comoving redshift prior")
        if empty_catalogue_file=='None':
            z = uniform_comoving_sampling(num,z_min,z_max,cosmo,bbh_evolution)
        else:
            z = empty_catalogue_sampling(empty_catalogue_file,num,bbh_evolution)
        
        one = np.ones((num,))
        ra = rng.uniform(0, 2. * np.pi, size=(num,))
        dec = np.arccos(rng.uniform(-1., 1., size=(num,))) - np.pi / 2.
        print('The number of events generated: '+str(num))
        parameters =pd.DataFrame.from_dict({
            'mass_1': np.array(mass_1), 
            'mass_2': np.array(mass_2), 
            'redshift': z,
            'luminosity_distance': cosmo.luminosity_distance(z).value*one,
            'theta_jn': np.arccos(rng.uniform(-1., 1., size=(num,))),
            'dec': dec,
            'ra': ra,
            'psi': rng.uniform(0, 2. * np.pi, size=(num,)),
            'phase': rng.uniform(0, 2. * np.pi, size=(num,)),
            'geocent_time': rng.uniform(t_start, t_end, size=(num,))
        })
        
    else:
        z,ra,dec,gal_id = galaxy_catalogue_sampling(galaxy_catalogue_file,band,num,cosmo,bbh_evolution)
        one = np.ones((num,))
        parameters =pd.DataFrame.from_dict({
            'mass_1': np.array(mass_1), 
            'mass_2': np.array(mass_2), 
            'redshift': z,
            'luminosity_distance': cosmo.luminosity_distance(z).value*one,
            'theta_jn': np.arccos(rng.uniform(-1., 1., size=(num,))),
            'dec': dec,
            'ra': ra,
            'psi': rng.uniform(0, 2. * np.pi, size=(num,)),
            'phase': rng.uniform(0, 2. * np.pi, size=(num,)),
            'geocent_time': rng.uniform(t_start, t_end, size=(num,)),
            'gal_id': gal_id
        })
        
        
    pop_file_name = 'BBH_'+args.pop_id+'.hdf5'
    parameters.to_hdf(pop_file_name, mode='w', key='root')
    
    with h5py.File(pop_file_name, 'a') as hdf_file:
        args_dict = vars(args) 
        for key, value in args_dict.items():
            hdf_file.attrs[key] = value

if __name__ == "__main__":
    main()

