#!/usr/bin/env python3
"""
Generate BBH samples for injections for GWFish
Zhuotao Li
"""

import gwcosmo
import numpy as np
import json
import pickle
import h5py
import pandas as pd
import os
import sys
import argparse
import datetime

from itertools import product

from gwcosmo.utilities.posterior_utilities import str2bool
from gwcosmo.utilities.arguments import create_parser
from gwcosmo.utilities.cosmology import *
from gwcosmo.utilities.check_boundary import *
from gwcosmo.injections import injections_at_detector

from gwcosmo.prior.priors import *
from astropy.cosmology import FlatLambdaCDM

from scipy import integrate
from scipy import interpolate







def condition_func_m1m2(reference_params, mass_1, mmin_det):
    return dict(minimum=mmin_det, maximum=mass_1)
    
class Injection_samples(object):
    def __init__(self,mmin_det,mmax_det,step_m,power_index_m1_det,power_index_m2_det,step_z,z_max,cosmo,cosmo_sampling,bbh_evolution):
        self.mmin_det = mmin_det
        self.mmax_det = mmax_det
        self.step_m = step_m
        self.power_index_m1_det = power_index_m1_det
        self.power_index_m2_det = power_index_m2_det
        self.step_z = step_z
        self.z_max = z_max
        self.bbh_evolution = bbh_evolution
        	
        self.bbh_pop = BBH_powerlaw(mminbh=mmin_det,mmaxbh=mmax_det,alpha=power_index_m1_det,beta=power_index_m2_det)
                                              
                                  
        self.cosmo = cosmo
        self.cosmo_sampling = cosmo_sampling
        self.z_comoving = np.linspace(self.step_z, self.z_max, round(self.z_max/self.step_z))
        
        if self.bbh_evolution == 'None':
            bbh_evo = 1/(1+self.z_comoving)
        self.z_pdf = np.array(self.cosmo_sampling.differential_comoving_volume(self.z_comoving).value)*bbh_evo
        norm_fac = sum(self.z_pdf)*self.step_z
        self.z_pdf = self.z_pdf/norm_fac
        
        
    def uniform_comoving_z_sampling(self,num):
        z_comoving = self.z_comoving
        z_max = self.z_max
        z_pdf = self.z_pdf
        step_z = self.step_z
        ########################
        event_count = 0
        samp_num = num
        z = []
        while event_count<num:
            uniform_samples_x = (z_max)*np.random.rand(samp_num)
            uniform_samples_y = max(z_pdf)*np.random.rand(samp_num)
            pdf_at_sample = interpolate.CubicSpline(z_comoving,z_pdf)(uniform_samples_x)
            ind = np.where(uniform_samples_y<pdf_at_sample)
            z_tmp = uniform_samples_x[ind]
            z = np.concatenate((z, z_tmp), axis=0)
            event_count += len(z_tmp)
            samp_num = num-len(z)
            
        return z
    
    def inver_cdf_z_sampling(self,num):
        z_comoving = self.z_comoving
        z_max = self.z_max
        z_pdf = self.z_pdf
        step_z = self.step_z
        
        z_cdf = np.cumsum(z_pdf)*step_z
        cdf_inver = np.linspace(0,1,1000)
        z_cdf_inverse = interpolate.CubicSpline(z_cdf,z_comoving)(cdf_inver)
        uniform_samples = np.random.rand(num)
        z = interpolate.CubicSpline(cdf_inver,z_cdf_inverse)(uniform_samples)
        return z
        

    def draw_samples(self, num):
        mass_1,mass_2 = self.bbh_pop.sample(num)          
        #mass_1 = (self.mmax_det-self.mmin_det)*np.random.rand(num) + self.mmin_det
        #mass_2 = (mass_1-np.ones(num)*self.mmin_det)*np.random.rand(num) + self.mmin_det

        z = self.uniform_comoving_z_sampling(num)
        dL= self.cosmo.dgw_z(z)
        
        samples = {'mass_1':mass_1,'mass_2': mass_2,'dL': dL,'z':z}
        
        return samples
        
    
    def prob_product(self,samples):
        
        mass_1 = samples['mass_1']
        mass_2 = samples['mass_2']
        z = samples['z']
        dL = samples['dL']
        p_z = interpolate.CubicSpline(self.z_comoving,self.z_pdf)(z)
        jacobian_zdL = 1/cosmo.ddgw_dz(z) # dz/ddL = 1/(ddL/dz)
        p_product = np.zeros(len(z))
        for i in range(len(z)):
            if mass_1[i]<mass_2[i]:
                p_product[i]=0
            else:
                mass_2_prior = np.nan_to_num(mass_2[i] ** self.power_index_m2_det * (1 + self.power_index_m2_det) /(mass_1[i] ** (1 + self.power_index_m2_det) - self.mmin_det ** (1 + self.power_index_m2_det)))
                #mass_1_prior =mass_1[i] ** self.power_index_m1_det * (1 + self.power_index_m1_det) /(self.mmax_det ** (1 + self.power_index_m1_det) - self.mmin_det ** (1 + self.power_index_m1_det))
                p_product[i] = p_z[i]*jacobian_zdL[i]*np.array(self.bbh_pop['mass_1'].prob(mass_1[i]))*mass_2_prior
                #p_product[i] = p_z[i]*jacobian_zdL[i]*mass_1_prior*mass_2_prior
                #p_product[i] = p_z[i]*jacobian_zdL[i]*(1/(self.mmax_det-self.mmin_det))*(1/(mass_1[i]-self.mmin_det))
        return p_product
        
    def calc_prob(self,samples):
        
        mass_1 = samples['mass_1']
        mass_2 = samples['mass_2']
        z = samples['z']
        dL = samples['dL']
        #p_z = interpolate.CubicSpline(self.z_comoving,self.z_pdf)(z)
        #jacobian_zdL = abs(1/self.cosmo.ddgw_dz(z)) # dz/ddL = 1/(ddL/dz)
        #p_dL = p_z*jacobian_zdL
        
        dL_list = self.cosmo.dl_zH0(self.z_comoving)
        jacobian_zdL_list = abs(1/self.cosmo.ddgw_dz(self.z_comoving))
        p_dL_list = self.z_pdf*jacobian_zdL_list
        dL_prior_norm = np.sum((p_dL_list[:-1]+p_dL_list[1:])*(dL_list[1:]-dL_list[:-1])/2)
        p_dL_list = p_dL_list/dL_prior_norm
        p_dL = interpolate.CubicSpline(dL_list,p_dL_list)(dL)
        
        mass_prior = np.nan_to_num(self.bbh_pop.joint_prob(mass_1,mass_2))
        pini = p_dL*mass_prior
        #norm_fac = self.normalize_constraint_factor(tuple(samples.keys()))
        return pini
        
        
    def normalize_constraint_factor(
    self,keys, sampling_chunk=500000, nrepeats=10
    ):
        factor_estimates = [
            self._estimate_normalization(keys, sampling_chunk)
            for _ in range(nrepeats)
        ]
        factor = np.mean(factor_estimates)
        if np.std(factor_estimates) > 0:
            decimals = int(-np.floor(np.log10(3 * np.std(factor_estimates))))
            factor_rounded = np.round(factor, decimals)
        else:
            factor_rounded = factor
        print(factor_estimates)
        return factor_rounded

    def _estimate_normalization(self,keys, sampling_chunk):
        dL_max = self.cosmo.dgw_z(self.z_max)
        samples = {key: np.array([]) for key in keys}
        #self.mmax_det=100
        samples['mass_1'] = (self.mmax_det-self.mmin_det)*np.random.rand(sampling_chunk) + self.mmin_det
        samples['mass_2'] = (self.mmax_det-self.mmin_det)*np.random.rand(sampling_chunk) + self.mmin_det
        samples['dL'] = dL_max*np.random.rand(sampling_chunk)
        samples['z'] = self.cosmo.z_dgw(samples['dL'])
        keep = np.array(self.prob_product(samples))
        
        volume = ((self.mmax_det-self.mmin_det)**2)*dL_max
        factor = volume*sum(keep)/len(keep)
        return factor


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--inj_num', type=int, default=int(1e6),
        help='Total number of the population for injection.')
    parser.add_argument(
        '--inj_id', type=str, default=datetime.datetime.now().strftime("%d%m%Y"),
        help='The name for the injection. Default is the date.')
    parser.add_argument(
        '--bbh_evolution', type=str, default='None',
        help='The time evolution model for BBHs.')
    parser.add_argument(
        '--step_z', type=float, default=0.01,
        help='Resolution the redshift distribution of the population.')
    parser.add_argument(
        '--z_max', type=float, default=4,
        help='Maximum of the redshift range of the population.')
    parser.add_argument(
        '--H0', type=float, default=70.0,
        help='Value of the Hubble constant.')
    parser.add_argument(
        '--Omega_M', type=float, default=0.25,
        help='Value of the Omega M. Although the default for GWTC3 is 0.3065, here we set the default to 0.25 according to the MICECATv2 mock catalogue.')
    parser.add_argument(
        '--mmin_det', type=float, default=1,
        help='Minimum value for black hole detector frame mass prior, using powerlaw.')
    parser.add_argument(
        '--mmax_det', type=float, default=500,
        help='Maximum value for black hole detector frame mass prior, using powerlaw.')
    parser.add_argument(
        '--step_m', type=float, default=1,
        help='Resolution the mass distribution of the population.')
    parser.add_argument(
        '--power_index_m1_det', type=float, default=2,
        help='Value of the m1 mass prior powerlaw index.')
    parser.add_argument(
        '--power_index_m2_det', type=float, default=0,
        help='Value of the m2 mass prior powerlaw index.')
    parser.add_argument(
        '--t_start', type=int, default=0,
        help='Value of the start time.')
    parser.add_argument(
        '--t_end', type=int, default=31536000,
        help='Value of the end time.')
    

    args = parser.parse_args()
    H0 = args.H0
    Omega_M = args.Omega_M                                 
    rng = np.random.default_rng()
    num = args.inj_num
    one = np.ones((num,))
    mmin_det = args.mmin_det
    mmax_det = args.mmax_det
    step_m = args.step_m
    power_index_m1_det = args.power_index_m1_det
    power_index_m2_det = args.power_index_m2_det
    step_z = args.step_z
    z_max = args.z_max
    bbh_evolution = args.bbh_evolution
    
    cosmo = gwcosmo.utilities.cosmology.standard_cosmology(H0=H0,Omega_m=Omega_M)  
    cosmo_sampling = FlatLambdaCDM(H0,Omega_M)
    #mass_1 = np.random.power(power_index_m1_det+1,num)*(mmax_det - mmin_det) + mmin_det
    #mass_ratio = np.random.power(power_index_m2_det+1,num)
    #mass_2 = mass_1*mass_ratio
    #mass_2 = np.random.power(power_index_m2_det+1,num)*(mass_1 - mmin_det) + mmin_det
    
    

    Injection = Injection_samples(mmin_det,mmax_det,step_m,power_index_m1_det,power_index_m2_det,step_z,z_max,cosmo,cosmo_sampling,bbh_evolution)
    samples = Injection.draw_samples(num)
    mass_1 = samples['mass_1']
    mass_2 = samples['mass_2']
    dL = samples['dL']
    z = samples['z']
    pini = Injection.calc_prob(samples)
    #p_prod = Injection.prob_product(samples)


    t_start = args.t_start
    t_end = args.t_end
    # redshift and cosmology here do not actually go into the GWFish SNR calculation
    parameters =pd.DataFrame.from_dict({
        'mass_1': np.array(mass_1)/(1+z), 
        'mass_2': np.array(mass_2)/(1+z), 
        'redshift': z*one,
        'luminosity_distance': dL*one,
        'theta_jn': np.arccos(rng.uniform(-1., 1., size=(num,))),
        'dec': np.arccos(rng.uniform(-1., 1., size=(num,))) - np.pi / 2.,
        'ra': rng.uniform(0, 2. * np.pi, size=(num,)),
        'psi': rng.uniform(0, 2. * np.pi, size=(num,)),
        'phase': rng.uniform(0, 2. * np.pi, size=(num,)),
        'geocent_time': rng.uniform(t_start, t_end, size=(num,)), # full year 2035
        'pini' : pini,
        #'p_prod' : p_prod,
        'Tobs' : (t_end-t_start)/3600/24/365.25
    })
    inj_file_name = 'Injection_'+str(num)+'_'+args.inj_id+'.hdf5'
    parameters.to_hdf(inj_file_name, mode='w', key='root')
    with h5py.File(inj_file_name, 'a') as hdf_file:
        args_dict = vars(args) 
        for key, value in args_dict.items():
            hdf_file.attrs[key] = value
    
    #parameters.to_hdf('test.hdf5', mode='w', key='root')

if __name__ == "__main__":
    main()

